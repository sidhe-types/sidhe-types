import { test } from "tap";
import { Interface } from "../../src/index.js";
import { Float32, Uint8, str } from "../../src/index.js";

test("Interface > properties > get", (t) => {
  class MyInterface extends Interface {
    foo = str(8);
    bar = Uint8;
    foobar = Float32;
  }
  class MyClass extends MyInterface {
    foo = "Hello";
    bar = 3;
    foobar = 3.1;
  }

  const myInstance = new MyClass();

  t.equal(myInstance.foo, "Hello", 'Str property is ("Hello")');
  t.equal(myInstance.bar, 3, "Uint8 property is (3)");
  t.equal(myInstance.foobar, 3.1, "Float32 property is (3.1)");

  t.end();
});

test("Interface > properties > set", (t) => {
  class MyClass extends Interface {
    foo = str(8);
    bar = Uint8;
    foobar = Float32;
  }

  const myInstance = new MyClass();
  myInstance.foo = "Hello";
  myInstance.bar = 3;
  myInstance.foobar = 3.1;

  t.equal(myInstance.foo, "Hello", 'Str property is ("Hello")');
  t.equal(myInstance.bar, 3, "Uint8 property is (3)");
  t.equal(myInstance.foobar, 3.1, "Float32 property is (3.1)");

  t.end();
});
