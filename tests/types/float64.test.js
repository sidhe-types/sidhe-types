import { Float64 } from "../../src/index.js";
import { testFloat } from "./lib/float.js";
import { NumericTestCase } from "./lib/numeric.js";

testFloat(
  new NumericTestCase(
    Float64,
    "Float64",
    Number.MIN_SAFE_INTEGER + 1,
    `${Number.MIN_SAFE_INTEGER + 1}`,
    Number.MIN_SAFE_INTEGER,
    `${Number.MIN_SAFE_INTEGER}`,
    Number.MAX_SAFE_INTEGER - 1,
    `${Number.MAX_SAFE_INTEGER - 1}`,
    Number.MAX_SAFE_INTEGER,
    `${Number.MAX_SAFE_INTEGER}`,
  ),
);
