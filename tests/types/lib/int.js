import {
  testNumericGetter,
  testNumericSetter,
  testNumericTester,
  testNumericValidator,
} from "./numeric.js";

export const testInt = (testCase) => {
  testNumericGetter(testCase);
  testNumericSetter(testCase);
  testNumericTester(testCase);
  testNumericValidator(testCase);
};
