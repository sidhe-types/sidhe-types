import { test } from "tap";
import {
  testNumericGetter,
  testNumericSetter,
  testNumericTester,
  testNumericValidator,
} from "./numeric.js";
import { $get, $set } from "../../../src/index.js";

export const testFloat = (testCase) => {
  testNumericGetter(testCase);
  test(`${testCase.name}.prototype.get`, (t) => {
    const intInstance = new testCase.class(1.1);

    t.equal(intInstance[$get](), 1.1, "gets (1.1)");

    t.end();
  });

  testNumericSetter(testCase);
  test(`${testCase.name}.prototype.set`, (t) => {
    const intInstance = new testCase.class(1);

    intInstance[$set](2.1);
    t.equal(intInstance[$get](), 2.1, "sets to (2.1)");

    intInstance[$set](-2.1);
    t.equal(intInstance[$get](), -2.1, "sets to (-2.1)");

    t.end();
  });

  testNumericTester(testCase);
  testNumericValidator(testCase);
};
