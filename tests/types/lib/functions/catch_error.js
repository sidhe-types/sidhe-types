export const catchError = (fn) => {
  let error = null;
  try {
    fn();
  } catch (e) {
    error = e;
  }

  return error;
};
