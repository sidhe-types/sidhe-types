import { Int8 } from "../../src/index.js";
import { testInt } from "./lib/int.js";
import { NumericTestCase } from "./lib/numeric.js";

testInt(
  new NumericTestCase(
    Int8,
    "Int8",
    -128,
    "-128",
    -129,
    "-129",
    127,
    "127",
    128,
    "128",
  ),
);
