import { Uint16 } from "../../src/index.js";
import { testInt } from "./lib/int.js";
import { NumericTestCase } from "./lib/numeric.js";

testInt(
  new NumericTestCase(
    Uint16,
    "Uint16",
    0,
    "0",
    -1,
    "-1",
    65535,
    "65535",
    65536,
    "65536",
  ),
);
