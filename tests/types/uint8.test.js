import { Uint8 } from "../../src/index.js";
import { testInt } from "./lib/int.js";
import { NumericTestCase } from "./lib/numeric.js";

testInt(
  new NumericTestCase(Uint8, "Uint8", 0, "0", -1, "-1", 255, "255", 256, "256"),
);
