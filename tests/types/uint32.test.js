import { Uint32 } from "../../src/index.js";
import { testInt } from "./lib/int.js";
import { NumericTestCase } from "./lib/numeric.js";

testInt(
  new NumericTestCase(
    Uint32,
    "Uint32",
    0,
    "0",
    -1,
    "-1",
    4294967295,
    "4294967295",
    4294967296,
    "4294967296",
  ),
);
