import { APACHE2_LICENSE } from "./licenses/apache-2.0.js";

export const SUPPORTED_LICENSES = new Map([["Apache-2.0", APACHE2_LICENSE]]);
