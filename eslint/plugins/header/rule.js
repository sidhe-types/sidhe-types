/*
 * Copyright 2023 Hasnae Rehioui (https://github.com/viqueen/eslint-plugin/tree/main/src/license-notice-rule)
 * Copyright 2023 Ronald M Zielaznicki
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { SUPPORTED_LICENSES } from "./supported_licenses.js";

export const licenseHeaderRule = {
  meta: {
    docs: {
      description: "include license header",
    },
    fixable: "code",
    messages: {
      "found-email-template":
        "copyright [email] template found, replace [email] with valid email or remove it from header",
      "found-name-template":
        "copyright [name] template found, replace [name] with copyright holder's name",
      "found-url-template":
        "copyright [url] template found, replace [url] with valid url or remove it from header",
      "found-year-template":
        "copyright [yyyy] template found, replace [yyyy] with copyright year.",
      "invalid-config-license-name-required":
        "invalid eslint configuration, license name is required",
      "malformed-license":
        "licence found, but it did not match the expected license.\n  actual: {{actual}}\n  expected: {{expected}}",
      "missing-header": "missing {{license}} header",
      "missing-license": "header comment missing {{license}} license",
    },
    schema: [
      {
        enum: [...SUPPORTED_LICENSES.keys()],
      },
    ],
    type: "suggestion",
  },
  create(context) {
    return {
      Program: handleProgram(context),
    };
  },
};

function handleProgram(context) {
  return (node) => {
    const [licenseName] = context.options;

    if (licenseName === undefined) {
      context.report({
        node,
        messageId: "invalid-config-license-name-required",
      });
      return;
    }

    // .js, .jsx, .ts, and .tsx file extensions
    const pattern = /^.*\.([jt])sx?$/;
    if (context.filename.match(pattern) === false) {
      return;
    }

    const comments = context.sourceCode.getCommentsBefore(node);
    const headerComment = comments.find((comment) => comment.type === "Block");
    if (headerComment === undefined) {
      context.report({
        node,
        messageId: "missing-header",
        data: { license: licenseName },
        fix(fixer) {
          fixer.insertTextBefore(
            node,
            [
              "/*",
              " * Copyright [yyyy] [name] <[email]> ([url])",
              " *",
              SUPPORTED_LICENSES.get(licenseName).split("\n").join("\n * "),
              " */",
            ].join("\n"),
          );
        },
      });
      return;
    }

    if (headerComment.value.includes("[yyyy]")) {
      context.report({
        node,
        messageId: "found-year-template",
      });
    }

    if (headerComment.value.includes("[name]")) {
      context.report({
        node,
        messageId: "found-name-template",
      });
    }

    if (headerComment.value.includes("[email]")) {
      context.report({
        node,
        messageId: "found-email-template",
      });
    }

    if (headerComment.value.includes("[url]")) {
      context.report({
        node,
        messageId: "found-url-template",
      });
    }

    const expectedSplitComment =
      SUPPORTED_LICENSES.get(licenseName).split("\n");
    const indexOfLicenseStart = headerComment.value.indexOf(
      expectedSplitComment[0],
    );
    if (indexOfLicenseStart === -1) {
      context.report({
        node,
        messageId: "missing-license",
        data: { license: licenseName },
      });
      return;
    }

    const actualSplitComment = headerComment.value
      .substring(indexOfLicenseStart)
      .split("\n");
    var actualHasExpected;
    for (let i = 0; i < expectedSplitComment.length; i++) {
      actualHasExpected = actualSplitComment[i].includes(
        expectedSplitComment[i],
      );

      if (actualHasExpected === false) {
        context.report({
          node,
          messageId: "malformed-license",
          data: {
            actual: actualSplitComment[i],
            expected: expectedSplitComment[i],
          },
        });
        return;
      }
    }
  };
}
