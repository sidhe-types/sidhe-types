import { licenseHeaderRule } from "./rule.js";

export default {
  rules: {
    "license-header": licenseHeaderRule,
  },
};
