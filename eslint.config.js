import licenseHeaderPlugin from "./eslint/plugins/header/index.js";

export default [
  {
    files: ["eslint/**/*.js", "src/**/*.js", "test/**/*.js"],
    languageOptions: {
      ecmaVersion: "latest",
      sourceType: "module",
    },
    plugins: {
      "license-header": licenseHeaderPlugin,
    },
    rules: {
      camelcase: "error",
      complexity: ["error", 6],
      "constructor-super": "error",
      "max-lines": ["error", 200],
      "multiline-comment-style": ["error", "starred-block"],
      "new-cap": "error",
      "no-constant-condition": "error",
      "no-dupe-else-if": "error",
      "no-ex-assign": "error",
      "no-irregular-whitespace": "error",
      "no-global-assign": "error",
      "no-new-symbol": "error",
      "no-self-assign": "error",
      "no-setter-return": "error",
      "no-sparse-arrays": "error",
      "no-this-before-super": "error",
      "no-unused-private-class-members": "error",
      "no-unused-vars": "error",
      "use-isnan": "error",
      "valid-typeof": "error",

      "license-header/license-header": ["error", "Apache-2.0"],
    },
  },
];
