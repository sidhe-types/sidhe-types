/*
 * Copyright 2023 Ronald M Zielaznicki <ronald.m.zielaznicki@git.revtts.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export class Num extends TypedPrimitive {
  static [$test](lowerBound, upperBound, val) {
    return lowerBound <= val && val <= upperBound;
  }

  static [$validate](lowerBound, upperBound, className, typeName, val) {
    if (Num[$test](lowerBound, upperBound, val) === false) {
      throw new OutOfBoundsTypedError(
        className,
        typeName,
        lowerBound,
        upperBound,
        val,
      );
    }
  }
}

import { TypedPrimitive } from "../typed/primitive.js";
import { OutOfBoundsTypedError } from "./errors/out_of_bounds.js";

import { $validate } from "../typed/symbols/validate.js";
import { $test } from "../typed/symbols/test.js";
