/*
 * Copyright 2023 Ronald M Zielaznicki <ronald.m.zielaznicki@git.revtts.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export class TypedObject extends Typed {
  [$typedInstances] = new Map();
  [$typedProperties] = new Map();

  constructor() {
    super();

    return new Proxy(this, {
      defineProperty,
      deleteProperty,
    });
  }

  [$getHook]() {
    return this;
  }

  [$setHook](newVal) {
    this[$dataView] = new DataView(
      newVal[$dataView].buffer,
      newVal[$dataView].byteOffset,
      newVal[$dataView].byteLength,
    );
    this[$typedView] = new Uint8Array(
      this[$dataView].buffer,
      this[$dataView].byteOffset,
      this[$dataView].byteLength,
    );

    for (let typedInstance of this[$typedInstances].values) {
      typedInstance[$reclaim](this[$dataView], this);
    }
  }

  [$setup](initVal, dataView, offset, owner) {
    var byteLength = 0;
    for (let [, TypedClass] of this[$typedProperties]) {
      byteLength += TypedClass[$byteLength];
    }
    this[$byteLength] = byteLength;
    super[$setup](initVal, dataView, offset, owner);

    var bufferOffset = 0;
    var typedInstance = null;
    for (let [key, TypedClass] of this[$typedProperties]) {
      typedInstance = new TypedClass(
        undefined,
        this[$dataView],
        bufferOffset,
        this,
      );
      Reflect.defineProperty(this, key, {
        get: typedInstance[$get].bind(typedInstance),
        set: typedInstance[$set].bind(typedInstance),
        configurable: true,
        enumerable: true,
      });

      this[$typedInstances].set(key, typedInstance);
      bufferOffset += typedInstance[$byteLength];
    }
  }

  [$test](val) {
    return typeof val === "object" && val instanceof this.constructor;
  }
}

import { defineProperty } from "./proxy/define_property.js";
import { deleteProperty } from "./proxy/delete_property.js";
import { $typedInstances } from './symbols/typed_instances.js';
import { $typedProperties } from './symbols/typed_properties.js';
import { $byteLength } from '../typed/symbols/byte_length.js';
import { $get } from '../typed/symbols/get.js';
import { $getHook } from "../typed/symbols/get_hook.js";
import { $dataView } from '../typed/symbols/data_view.js';
import { $reclaim } from "../typed/symbols/reclaim.js";
import { $set } from '../typed/symbols/set.js';
import { $setHook } from '../typed/symbols/set_hook.js';
import { $setup } from '../typed/symbols/setup.js';
import { $test } from '../typed/symbols/test.js';
import { $typedView } from '../typed/symbols/typed_view.js';
import { Typed } from "../typed/typed.js";
