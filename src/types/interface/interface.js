/*
 * Copyright 2023 Ronald M Zielaznicki <ronald.m.zielaznicki@git.revtts.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export class Interface extends TypedObject {

  [$setup](initVal, dataView, offset, owner) {
    super[$setup](initVal, dataView, offset, owner)
    this[$typeIsInit] = true;
  }

  [$validate](val) {
    if (this.test(val) === false) {
      throw new IncorrectTypeTypedError(
        this.constructor.name,
        "Interface",
        val,
      );
    }
  }
}

import { $setup } from '../../lib/types/typed/symbols/setup.js';
import { $typeIsInit } from '../../lib/types/typed/symbols/type_is_init.js';
import { $validate } from "../../lib/types/typed/symbols/validate.js";
import { IncorrectTypeTypedError } from "../../lib/types/typed/errors/incorrect_type.js";
import { TypedObject } from "../../lib/types/object/object.js";
